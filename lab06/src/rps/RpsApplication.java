//Mandeep Singh 1937332
package rps;

import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;


public class RpsApplication extends Application {
	//Private RpsGame Object
	private RpsGame game = new RpsGame();
	
	public void start(Stage stage) {
		//create the root of the scene
		Group root = new Group(); 
		//Create buttons into Hbox
		HBox buttons = new HBox();
		Button rockButton = new Button("Rock");
		Button paperButton = new Button("Paper");
		Button scissorsButton = new Button("Scissors");
		buttons.getChildren().addAll(rockButton,paperButton,scissorsButton);
		//Create text fields into Hbox
	    HBox textFields = new HBox();
	    TextField message = new TextField("Welcome to Rock Paper Scissors!");
	    TextField wins = new TextField("Wins: 0");
	    TextField losses = new TextField("Losses: 0");
	    TextField ties =  new TextField("Ties: 0");
		//Set all onAction buttons
	    rockButton.setOnAction(new RpsChoice(message,wins,losses,ties,"Rock",game));
	    paperButton.setOnAction(new RpsChoice(message,wins,losses,ties,"Paper",game));
	    scissorsButton.setOnAction(new RpsChoice(message,wins,losses,ties,"Scissors",game));
	    //setWidth of message
	    //200px was too small, 265px is just right
	    message.setPrefWidth(265);
	    //add all textFields into HBox
	    textFields.getChildren().addAll(message,wins,losses,ties);
	    //Adding both hbox to vbox
	    VBox overall = new VBox();
	    overall.getChildren().addAll(buttons,textFields);
	    //Add vbox to root
	    root.getChildren().add(overall);
		//scene is associated with container, dimensions
		Scene scene = new Scene(root, 650, 300); 
		scene.setFill(Color.BLACK);
		//associate scene to stage and show
		stage.setTitle("Rock Paper Scissors"); 
		stage.setScene(scene); 
		
		stage.show(); 

	}
	
    public static void main(String[] args) {
        Application.launch(args);
    }
}    

