//Mandeep Singh 1937332
package rps;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;

public class RpsChoice implements EventHandler<ActionEvent> {
	//Private fields
	private TextField message;
	private TextField wins;
	private TextField losses;
	private TextField ties;
	private String playerChoice;
	private RpsGame gameObject;
	
	//Contructor with 6 inputs
	public RpsChoice(TextField message, TextField wins, TextField losses, TextField ties, String playerChoice, RpsGame gameObject) {
		this.message = message;
		this.wins = wins;
		this.losses = losses;
		this.ties = ties;
		this.playerChoice = playerChoice;
		this.gameObject = gameObject;	
	}
	
	public void handle(ActionEvent e) {
	    String result =gameObject.playRound(this.playerChoice);
	    this.message.setText(result);
	    this.wins.setText("Wins:"+gameObject.getWins());
	    this.ties.setText("Ties:"+gameObject.getTies());
	    this.losses.setText("Losses:"+gameObject.getLosses());
		
	}

}
