//Mandeep Singh 1937332
package rps;

import java.util.*;

public class RpsGame {
	//private fields
	private int wins = 0;
	private int ties = 0;
	private int losses = 0;
	private Random randGen = new Random();
	//default contructor
	public RpsGame() {}
	//getters
	public int getWins() {return this.wins;}
	public int getTies() {return this.ties;}
	public int getLosses() {return this.losses;}
	
	public String playRound(String playerChoice) {
		
		//Uppercase the first letter and lowercase the rest
		playerChoice = playerChoice.substring(0,1).toUpperCase() + playerChoice.substring(1).toLowerCase();
		//Rock = 0, Scissors = 1 , Paper = 2
		
		int computerRandomChoice = randGen.nextInt(3);
		String computerChoice ="";
		String gameResult ="";
		
		switch(computerRandomChoice) {
		
			case 0:  computerChoice = "Rock";
					 break;
			case 1:  computerChoice = "Scissors";
					 break;
			case 2:  computerChoice = "Paper";
					 break;
		
		}
		//Tie possibility
		if(playerChoice.equals(computerChoice)) {
			gameResult = "Computer plays "+computerChoice+" and the game is a tie!";
			ties++;
		}
		//Player wins possibilities
		else if(playerChoice.equals("Rock") && computerChoice.equals("Scissors")) {
			gameResult = "Computer plays "+computerChoice+" and the player won!";
			wins++;	
		}
		else if(playerChoice.equals("Scissors") && computerChoice.equals("Paper")) {
			gameResult = "Computer plays "+computerChoice+" and the player won!";
			wins++;
		}
		else if(playerChoice.equals("Paper") && computerChoice.equals("Rock")) {
			gameResult = "Computer plays "+computerChoice+" and the player won!";
			wins++;
		}
		//Computer wins possibilities
		else if(computerChoice.equals("Rock") && playerChoice.equals("Scissors")) {
			gameResult = "Computer plays "+computerChoice+" and the computer won!";
			losses++;	
		}
		else if(computerChoice.equals("Scissors") && playerChoice.equals("Paper")) {
			gameResult = "Computer plays "+computerChoice+" and the computer won!";
			losses++;
		}
		else if(computerChoice.equals("Paper") && playerChoice.equals("Rock")) {
			gameResult = "Computer plays "+computerChoice+" and the computer won!";
			losses++;
		}	
		
		else {
			System.out.print("Invalid player choice");
		}
		
		return gameResult;
	}
	

}
